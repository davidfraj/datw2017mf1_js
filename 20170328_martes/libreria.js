// Archivo de Libreria de funciones en Javascript
// Archivo con la extension .js
//Funcion dimeTitulo()
	// Recibe-> Un texto
	// Devuelve -> El texto formateado como titulo en HTML
function dimeTitulo(texto, n=3){
	//METODO 1:
	//Si NO es un NUMERO la variable n
	//isNotANumber
	// if(isNaN(n)){
	// 	n=3;
	// }

	// if(n>6){
	// 	n=6;
	// }else if(n<1){
	// 	n=1;
	// }

	//METODO 2
	switch(n){
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			break;
		default:
			n=3;
			break;
	}

	return '<h'+n+'>'+texto+'</h'+n+'>';
}

function dibujaCampo(titulo, tipo){
	r='';
	r+='<label>'+titulo+'</label>';
	r+='<input type="'+tipo+'" placeholder="'+titulo+'">';
	r+='<br>';
	return r;
}

function dibujaLista(num, tipo='desordenada'){
	r='';
	if(tipo=='ordenada'){
		inicio='<ol>';
		fin='</ol>';
	}else{
		inicio='<ul>';
		fin='</ul>';
	}
	r+=inicio;
	for(i=1;i<=num;i++){
		r+='<li>Elemento '+i+'</li>';
	}
	r+=fin;
	return r;
}

//funcion que recibe un vector de elementos, y devolvera una lista desordenada con dichos elementos
function dibujaVector(vector){
	r='';
	r+='<ul>';
	for(i=0;i<vector.length;i++){
		r+='<li>'+vector[i]+'</li>';
	}
	r+='<ul>';
	return r;
}