function dimeMayor(vector){
	//Me apunto el numero mayor:
	mayor=vector[0];
	for(i=1;i<vector.length;i++){
		if(vector[i]>mayor){
			mayor=vector[i];
		}
	}
	return mayor;
}

function dimeMenor(vector){
	//Me apunto el numero menor:
	menor=vector[0];
	for(i=1;i<vector.length;i++){
		if(vector[i]<menor){
			menor=vector[i];
		}
	}
	return menor;
}

function dimeSuma(vector){
	//Me creo una variable para ACUMULAR la suma:
	suma=0;
	for(i=0;i<vector.length;i++){
		suma+=vector[i];
	}
	return suma;
}

function dimeMedia(vector){
	return dimeSuma(vector)/vector.length;
}

function dimeMedia2(vector){
	suma=0;
	for(i=0;i<vector.length;i++){
		suma+=vector[i];
	}
	return suma/vector.length;
}

function muestraVector(vector){
	res='';
	for(i=0;i<vector.length;i++){
		res+=vector[i]+' ';
	}
	return res;
}

function ordenaVector(vector){
	for(j=0;j<vector.length-1;j++){
		for(i=0;i<vector.length-1-j;i++){
			if(vector[i]>vector[i+1]){
				aux=vector[i];
				vector[i]=vector[i+1];
				vector[i+1]=aux;
			}
		}
	}
	return vector;
}

function ordenaVectorF(vector){
	//cont=0;
	for(j=0;j<vector.length-1;j++){
		f=false;
		for(i=0;i<vector.length-1-j;i++){
			if(vector[i]>vector[i+1]){
				aux=vector[i];
				vector[i]=vector[i+1];
				vector[i+1]=aux;
				f=true;
			}
			//cont++;
			//document.write(cont+' ');
		}
		if(f==false){
			return vector;
		}
	}
	return vector;
}