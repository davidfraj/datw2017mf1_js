Lunes 20 de Marzo de 2017
-------------------------

Como escribir en lenguaje javascript
	<script type="text/javascript"></script>
	<script></script>

Variables en JAVASCRIPT
	numero=10;
	precio=20.5;
	texto='<p class="tipo1">Otro fragmento de texto</p>'; //SI
	texto2="<p class="tipo1">Otro fragmento de texto</p>"; //NO
	texto3="<p class='tipo1'>Otro fragmento de texto</p>"; //NO
	texto3="<p class=\"tipo1\">Otro fragmento de texto</p>"; //SI
	activado=true; //Tipo de dato BOOLEANO
	activado=false; //Tipo de dato BOOLEANO

Como escribir desde javascript en HTML
	document.write('Lo que sea');
	document.write('El valor de la variable precio es ' + precio);

Operadores dentro del lenguaje
	precio=100*2;
	precio=precio*1.21;
	precio=precio/5;
	precio=(120+70+50)/3;
	resto=150%3; //Operacion modulo o resto
		//Devolvemos el resto de una division entera

	Operaciones especiales
		numero=10;
		numero++; //POSTIncremento en 1 la variable numero
		numero=numero+1;
		numero+=7;
		numero=numero+7;
		++numero; //PREIncremento en 1 la variable numero

Estructuras de control
----------------------
	2 tipos de estructuras de control
		Condicionales
			Evalúan una expresion, para ver si se cumple o no (TRUE o FALSE)
				Dependiendo de la expresion, el lenguaje elije una acción u otra

		Repetitivas o de tipo BUCLE

	>	mayor que
	>= 	mayor o igual que
	<	menor que
	<=	menor o igual
	==	igual que (En valor)
	!=	distinto de
	===	igual ESTRICTO (En valor y tipo de datos)
	= 	Asignacion

	if(numero==5){

	}

//PROPONEMOS EJERCICIO
	importe=50000;
	//Si las ventas son de 20000
	porcentaje de ventas es del 2%

	//si las ventas son de 40000
	porcentaje de ventas es del 4%

	//si las ventas son mayores de 60000
	porcentaje de ventas es del 6%

adjuntamos 20170320_lunes_dav.rar